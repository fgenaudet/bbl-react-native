/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

import AppPage from './App/Pages/AppPage';

class bbl_app extends Component {
  render() {
    return (
      <AppPage />
    );
  }
}

AppRegistry.registerComponent('bbl_app', () => bbl_app);
