import React, {Component} from 'react';
import {View, Text, ScrollView, StyleSheet, Picker, RefreshControl, LayoutAnimation} from 'react-native';

// App
import BBLTheme from '../Theme/BBLTheme';
import TrolleysService from '../Services/TrolleyService';
import TrolleyList from '../Components/TrolleyList';
import Button from '../Components/Button';

// Styles
const styles = StyleSheet.create({
    page: {
        flex:1,
        paddingTop: 30
    },
    button_text: {fontWeight: 'bold'},
    button: {width: 100, height: 50, backgroundColor: '#f1f1f1', alignSelf: 'center', justifyContent: 'center', alignItems: 'center'}
});

export default class AppPage extends Component {
    constructor() {
        super();
        this.state = {
            trolleys: [],
            refreshing: false,
        }
    }
    componentWillUpdate() {
        LayoutAnimation.easeInEaseOut();
    }
    componentDidMount() {
        TrolleysService.all().then(trolleys => {
            this.setState({trolleys})
        })
    }
    render() {
        let refreshControl = <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh.bind(this)}
        />;

        return (
            <ScrollView style={styles.page} refreshControl={refreshControl}>
                <View>
                    <Picker
                        selectedValue={this.state.selectedFilter}
                        onValueChange={(filter) => this.setState({selectedFilter : filter})}>
                        <Picker.Item label="ALL" value="ALL" />
                        {
                            ["2012", "2011", "2010", "2009", "2008"].map((year,k) => <Picker.Item key={k} label={year} value={year} />)
                        }
                    </Picker>

                    <Button onPress={this._applyFilter.bind(this)} style={styles.button}>
                        <Text style={styles.button_text}>Apply</Text>
                    </Button>
                </View>
                <TrolleyList trolleys={this.state.trolleys} filter={this.state.filter}/>
            </ScrollView>
        )
    }

    _applyFilter() {
        this.setState({filter: this._getFilterFromValue(this.state.selectedFilter)})
    }
    _getFilterFromValue(value) {
        switch (value) {
            case 'ALL': return f => f;
            default: return f => f.year === value;
        }
    }
    _onRefresh() {
        this.setState({refreshing: true, trolleys: []});
        setTimeout(() => {
            TrolleysService.all().then(trolleys => {
                this.setState({trolleys, refreshing: false})
            });
        }, 2000)

    }
}
