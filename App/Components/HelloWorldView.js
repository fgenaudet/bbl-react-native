import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

// Theme
import BBLTheme from '../Theme/BBLTheme';

export default class HelloWorldView extends Component {
    render() {
        return (
            <View style={BBLTheme.page}>
                <Text style={BBLTheme.text_title}>{this._reverse(this.props.text)}</Text>
            </View>
        )
    }
    _reverse(s: string) :string{
        return s.split("").reverse().join("");
    }
}

HelloWorldView.propTypes = {
    text: React.PropTypes.string.isRequired
};
HelloWorldView.defaultProps = {
    text: `I'm a default text`
};
