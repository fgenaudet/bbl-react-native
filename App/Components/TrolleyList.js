import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

// App
import TrolleyContainer from './TrolleyContainer';

// Styles
const styles = StyleSheet.create({
    trolley_container: {
        flexDirection: 'row',
        backgroundColor: '#f1f1f1',
        marginHorizontal: 15,
        marginVertical: 5,
        paddingVertical: 7
    },
    trolley_year: {
        fontWeight: 'bold',
        fontSize: 20,
        width: 70
    },
    trolley_city: {
        fontSize: 16
    }
});

export default class TrolleyList extends Component {
    render() {
        let filter = this.props.filter ? this.props.filter : f => f;
        let trolleys = this.props.trolleys.filter(filter);
        return (
            <View>
                {
                    trolleys.map((trolley, i) => {
                        return (
                            <TrolleyContainer key={i} style={styles.trolley_container} number_of_trolleys={trolley.number_of_trolleys}>
                                <Text style={styles.trolley_year}>{trolley.year}</Text>
                                <View>
                                    <Text style={styles.trolley_city}>{trolley.river}</Text>
                                    <Text>{trolley.number_of_trolleys} trolley(s) in the river</Text>
                                </View>
                            </TrolleyContainer>
                        )
                    })
                }
            </View>
        )
    }
}
