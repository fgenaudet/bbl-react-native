import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

// App

// Styles
const styles = StyleSheet.create({})

export default class TrolleyContainer extends Component {
    render() {
        return (
            <View style={[this.props.style, this._getBorderColor(this.props.number_of_trolleys)]}>
                {this.props.children}
            </View>
        )
    }

    _getBorderColor(nbTrolley) {
        let color = 'black';
        if (nbTrolley == 0) {
            color = 'green';
        } else if (nbTrolley <= 10) {
            color = 'blue';
        } else {
            color = 'red';
        }
        return {borderBottomColor: color, borderBottomWidth: 2};
    }
}