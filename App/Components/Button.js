import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, TouchableNativeFeedback, Platform} from 'react-native';

// App

// Styles
const styles = StyleSheet.create({})

export default class Button extends Component {
    render() {
        return (
            <View>
                {
                    Platform.OS === 'ios' ?
                        <TouchableOpacity {...this.props} >
                            {this.props.children}
                        </TouchableOpacity> :

                        <TouchableNativeFeedback {...this.props}
                            background={TouchableNativeFeedback.SelectableBackground(0)}
                        >
                            {this.props.children}
                        </TouchableNativeFeedback>
                }
            </View>
        )
    }
}