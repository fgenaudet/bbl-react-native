export default class BBLTheme {
    static text_title =  {fontSize: 20, textAlign: 'center', fontWeight: 'bold'}
    static page = {flex: 1, backgroundColor: '#f1f1f1', justifyContent: 'center', alignItems: 'center'};
}